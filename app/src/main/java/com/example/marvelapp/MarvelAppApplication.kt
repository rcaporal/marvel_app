package com.example.marvelapp

import android.app.Application
import com.example.marvelapp.di.applicationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MarvelAppApplication : Application() {

    override fun onCreate(){
        super.onCreate()
        startKoin {
            androidContext(this@MarvelAppApplication)
            modules(applicationModule)
        }
    }

}