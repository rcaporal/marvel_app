package com.example.marvelapp.util

import android.content.Context
import com.example.marvelapp.data.model.Keys
import com.google.gson.Gson
import java.io.IOException
import java.nio.charset.Charset

fun getKeys(context: Context): Keys = Gson().fromJson(context.loadJsonFromAsset("keys"), Keys::class.java)

fun Context.loadJsonFromAsset(fileName: String): String {
    val json: String?
    try {
        val inputStream = this.assets.open("$fileName.json")

        val size = inputStream.available()

        val buffer = ByteArray(size)

        inputStream.read(buffer)

        inputStream.close()

        json = String(buffer, Charset.defaultCharset())

    } catch (ex: IOException) {
        ex.printStackTrace()
        throw NoKeysFileException()
    }

    return json
}