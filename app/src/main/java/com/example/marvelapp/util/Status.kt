package com.example.marvelapp.util

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}