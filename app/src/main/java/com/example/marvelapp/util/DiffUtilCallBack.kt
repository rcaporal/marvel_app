package com.example.marvelapp.util

import androidx.recyclerview.widget.DiffUtil
import com.example.marvelapp.data.model.CharacterModel

class DiffUtilCallBack: DiffUtil.ItemCallback<CharacterModel>() {
    override fun areItemsTheSame(oldItem: CharacterModel, newItem: CharacterModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: CharacterModel, newItem: CharacterModel): Boolean {
        return oldItem.name == newItem.name
                && oldItem.id == newItem.id
                && oldItem.description == newItem.description
    }

}