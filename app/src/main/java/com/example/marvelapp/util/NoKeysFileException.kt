package com.example.marvelapp.util

import java.lang.RuntimeException

class NoKeysFileException : RuntimeException()