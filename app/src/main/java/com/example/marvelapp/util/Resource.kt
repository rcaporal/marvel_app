package com.example.marvelapp.util

import com.example.marvelapp.R
import java.io.IOException
import java.lang.Exception

data class Resource<out T>(val status: Status, val data: T?, val stringResource: Int?) {
    companion object {

        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(throwable: Throwable, data: T?): Resource<T> {
            val msg = when (throwable) {
                is IOException -> R.string.error_no_internet_connection
                else -> R.string.error_default
            }
            return Resource(Status.ERROR, data, msg)
        }

        fun <T> error(exception: Exception?): Resource<T> {
            val msg = when (exception) {
                null -> R.string.error_default
                is IOException -> R.string.error_no_internet_connection
                is NoKeysFileException -> R.string.no_keys_file_message
                else -> R.string.error_default
            }
            return Resource(Status.ERROR, null, msg)
        }

        fun <T> error(errorCode: Int, data: T? = null): Resource<T> {
            val msg = when(errorCode){
                401 -> R.string.error_not_authorized
                404 -> R.string.error_not_found
                else -> R.string.error_default
            }
            return Resource(Status.ERROR, data, msg)
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }
    }
}