package com.example.marvelapp.util

import android.view.View
import android.widget.ImageView
import com.squareup.picasso.Picasso
import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

fun List<String>.toMd5(): String? {
    return try {
        val md = MessageDigest.getInstance("MD5")

        var bytes: ByteArray = byteArrayOf()

        this.forEach {
            bytes += it.toByteArray()
        }

        val messageDigest = md.digest(bytes)

        val no = BigInteger(1, messageDigest)

        var hashtext = no.toString(16)
        while (hashtext.length < 32) {
            hashtext = "0$hashtext"
        }
        hashtext
    } catch (e: NoSuchAlgorithmException) { null }
}

fun View?.visible(visible: Boolean) {
    this?.visibility = if(visible) View.VISIBLE else View.GONE
}

fun ImageView?.load(imageUrl: String) {
    if (imageUrl.isNotEmpty() && this != null){
        Picasso.get()
            .load(imageUrl)
            .into(this)
    }
}