package com.example.marvelapp.view.activity

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.marvelapp.R
import com.example.marvelapp.data.model.CharacterModel
import com.example.marvelapp.util.Resource
import com.example.marvelapp.util.Status
import com.example.marvelapp.util.visible
import com.example.marvelapp.view.adapter.CharactersAdapter
import com.example.marvelapp.view.view_model.CharacterCardsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.loading_layout.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity() {

    private lateinit var charactersObserver: Observer<Resource<PagedList<CharacterModel>>>

    private val viewModel: CharacterCardsViewModel by viewModel()
    private val adapter: CharactersAdapter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter.onItemClick = { character ->
            onCharacterClick(character)
        }

        mainRecycler.layoutManager = LinearLayoutManager(this)
        mainRecycler.adapter = adapter

        initObservers()
        getData()
    }

    private fun onCharacterClick(character: CharacterModel) {
        CharacterActivity.open(this, character.id.toString(), character)
    }

    private fun getData() {
        viewModel.fetchCharacters().observe(this, charactersObserver)
    }

    private fun initObservers() {

        charactersObserver = Observer {
            when (it.status) {
                Status.LOADING -> {
                    loadingLayout?.visible(true)
                }
                Status.SUCCESS -> {
                    it.data?.let { characterList ->
                        adapter.submitList(characterList)
                        loadingLayout?.visible(false)
                    }
                }
                Status.ERROR -> {
                    it.stringResource?.let { strResource ->
                        showErrorDialog(
                            resources.getString(strResource),
                            onRetry = { viewModel.fetchCharacters(forceReload = true).observe(this, charactersObserver) }
                        )
                    }
                }
            }
        }

    }
}
