package com.example.marvelapp.view.activity

import androidx.appcompat.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.callbacks.onCancel
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.example.marvelapp.R

abstract class BaseActivity : AppCompatActivity() {

    internal fun showErrorDialog(errorMessage: String, onRetry: ()->Unit) {
        MaterialDialog(this).show {
            lifecycleOwner(this@BaseActivity)
            title(R.string.error)
            message (text = errorMessage)
            onCancel { finish() }
            positiveButton(R.string.retry_label) { onRetry() }
        }
    }

}