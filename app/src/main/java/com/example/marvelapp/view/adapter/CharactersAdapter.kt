package com.example.marvelapp.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.marvelapp.R
import com.example.marvelapp.data.model.CharacterModel
import com.example.marvelapp.util.DiffUtilCallBack
import com.example.marvelapp.util.load
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.card_character.*

class CharactersAdapter(private val context: Context):
    PagedListAdapter<CharacterModel, CharactersAdapter.CharactersViewHolder>(DiffUtilCallBack()) {

    var onItemClick: (character: CharacterModel) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharactersViewHolder {
        val inflater = LayoutInflater.from(context)
        val contactView = inflater.inflate(R.layout.card_character, parent, false)
        return CharactersViewHolder(contactView)
    }

    override fun onBindViewHolder(holder: CharactersViewHolder, position: Int) {
        getItem(position)?.let { holder.mount(it) }
    }

    inner class CharactersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

        override val containerView: View? = itemView

        fun mount(character: CharacterModel) {

            imageCharCard.load(character.thumbnail.imageUrl)

            titleCharCard.text = character.name

            rootCharCard.setOnClickListener { onItemClick(character) }

        }

    }
}