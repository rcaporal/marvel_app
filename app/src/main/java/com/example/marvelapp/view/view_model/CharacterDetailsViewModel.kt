package com.example.marvelapp.view.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.example.marvelapp.data.model.CharacterModel
import com.example.marvelapp.data.repository.CharactersRepository
import com.example.marvelapp.util.Resource

class CharacterDetailsViewModel(var repository: CharactersRepository) : ViewModel() {

    private lateinit var characterLiveData: LiveData<Resource<CharacterModel>>

    fun getCharacterById(id: String, forceReload: Boolean = false):LiveData<Resource<CharacterModel>>{

        return if (!::characterLiveData.isInitialized || forceReload){
            characterLiveData = liveData {

                emit(Resource.loading())

                try {
                    val model = repository.getCharacterById(id)

                    if (model == null){
                        emit(Resource.error(exception = null))
                    }else{
                        emit(Resource.success(model))
                    }
                } catch(exception: Exception) {
                    emit(Resource.error(exception))
                }
            }

            characterLiveData
        }else{
            characterLiveData
        }

    }
}