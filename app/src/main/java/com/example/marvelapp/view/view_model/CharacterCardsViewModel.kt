package com.example.marvelapp.view.view_model

import android.content.Context
import androidx.lifecycle.*
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.marvelapp.data.api.MarvelAPI
import com.example.marvelapp.data.datasource.CharactersDataSource
import com.example.marvelapp.data.model.CharacterModel
import com.example.marvelapp.util.Resource

class CharacterCardsViewModel(private val context: Context, private val api: MarvelAPI) :
    ViewModel() {

    private lateinit var charactersDataSourceLiveData: LiveData<PagedList<CharacterModel>>
    private lateinit var charactersSourceLiveData: MediatorLiveData<Resource<PagedList<CharacterModel>>>


    private fun initDataSource() {
        val config = PagedList.Config.Builder()
            .setPageSize(CharactersDataSource.PAGE_SIZE)
            .setEnablePlaceholders(false)
            .build()
        charactersDataSourceLiveData = initializedPagedListBuilder(config).build()
    }

    private fun initializedPagedListBuilder(config: PagedList.Config):
            LivePagedListBuilder<Int, CharacterModel> {

        val dataSourceFactory = object : DataSource.Factory<Int, CharacterModel>() {
            override fun create(): DataSource<Int, CharacterModel> {
                return CharactersDataSource(context, viewModelScope, api, onApiError = {
                    charactersSourceLiveData.postValue(Resource.error(it))
                })
            }
        }

        return LivePagedListBuilder<Int, CharacterModel>(dataSourceFactory, config)
    }

    fun fetchCharacters(forceReload: Boolean = false): LiveData<Resource<PagedList<CharacterModel>>> {
        return if (!::charactersSourceLiveData.isInitialized || forceReload){
            charactersSourceLiveData = MediatorLiveData()
            charactersSourceLiveData.postValue(Resource.loading())
            try {
                initDataSource()
                charactersSourceLiveData.addSource(charactersDataSourceLiveData){
                    charactersSourceLiveData.postValue(Resource.success(it))
                }
            }catch (e: Exception){
                charactersSourceLiveData.postValue(Resource.error(e))
            }
            charactersSourceLiveData
        }else{
            charactersSourceLiveData
        }
    }

}