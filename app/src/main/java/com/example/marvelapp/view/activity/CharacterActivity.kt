package com.example.marvelapp.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.example.marvelapp.R
import com.example.marvelapp.data.model.CharacterModel
import com.example.marvelapp.util.Resource
import com.example.marvelapp.util.Status
import com.example.marvelapp.util.load
import com.example.marvelapp.util.visible
import com.example.marvelapp.view.view_model.CharacterDetailsViewModel
import kotlinx.android.synthetic.main.activity_character.*
import kotlinx.android.synthetic.main.loading_layout.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class CharacterActivity : BaseActivity() {

    companion object{
        const val CHARACTER_ID_EXTRA = "CHARACTER_ID_EXTRA"
        const val CHARACTER_EXTRA = "CHARACTER_EXTRA"

        fun open(context: Context, characterId: String, character: CharacterModel? = null){
            val intent = Intent(context, CharacterActivity::class.java)
            intent.putExtra(CHARACTER_ID_EXTRA, characterId)
            character?.let {
                intent.putExtra(CHARACTER_EXTRA, character)
            }
            context.startActivity(intent)
        }
    }

    private lateinit var charactersObserver: Observer<Resource<CharacterModel>>
    private lateinit var characterId: String

    private val viewModel: CharacterDetailsViewModel by viewModel()
    private var character: CharacterModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character)
        configureToolbar()
        initByIntent(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun configureToolbar() {
        supportActionBar?.let { actionBar->
            val upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back)
            actionBar.setHomeAsUpIndicator(upArrow)
            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onNewIntent(newIntent: Intent?) {
        super.onNewIntent(newIntent)
        initByIntent(newIntent)
    }

    private fun initByIntent(newIntent: Intent?){
        if(newIntent != null){
            getIntentData()
            initObservers()
            getData()
        }else{
            showErrorDialog(resources.getString(R.string.error_default)) {finish()}
        }
    }

    private fun getIntentData() {
        characterId = intent.getStringExtra(CHARACTER_ID_EXTRA) ?: ""
        character = intent.getSerializableExtra(CHARACTER_EXTRA) as? CharacterModel
    }

    private fun getData(forceReload: Boolean = false) {
        if (character != null){
            mountCharacterViews(character!!)
        }else{
            viewModel.getCharacterById(characterId, forceReload).observe(this, charactersObserver)
        }
    }

    private fun initObservers() {

        charactersObserver = Observer {
            when (it.status) {
                Status.LOADING -> {
                    loadingLayout?.visible(true)
                }
                Status.SUCCESS -> {
                    it.data?.let { character ->
                        mountCharacterViews(character)
                    }
                }
                Status.ERROR -> {
                    it.stringResource?.let { strResource ->
                        showErrorDialog(
                            resources.getString(strResource),
                            onRetry = { getData(forceReload = true) }
                        )
                    }
                }
            }
        }

    }

    private fun mountCharacterViews(character: CharacterModel): Unit? {
        imageCharacter.load(character.thumbnail.imageUrl)
        textNameCharacter.text = character.name
        textDescriptionCharacter.text = if (character.description.isEmpty()) {
            resources.getString(R.string.no_description)
        } else character.description
        return loadingLayout?.visible(false)
    }
}
