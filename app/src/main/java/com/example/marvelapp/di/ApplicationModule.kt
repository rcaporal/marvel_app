package com.example.marvelapp.di

import com.example.marvelapp.data.api.MarvelAPI
import com.example.marvelapp.data.repository.CharactersRepository
import com.example.marvelapp.data.repository.implementation.CharactersRepositoryImpl
import com.example.marvelapp.view.adapter.CharactersAdapter
import com.example.marvelapp.view.view_model.CharacterCardsViewModel
import com.example.marvelapp.view.view_model.CharacterDetailsViewModel
import org.koin.dsl.module

val applicationModule = module {
    factory { MarvelAPI() }
    factory { CharactersAdapter(context = get()) }
    factory { CharacterCardsViewModel(context = get(), api = get()) }
    factory { CharacterDetailsViewModel(repository = get()) }
    single <CharactersRepository> { CharactersRepositoryImpl(context = get(), api = get()) }
}