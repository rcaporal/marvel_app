package com.example.marvelapp.data.model

import com.google.gson.annotations.SerializedName

data class CharactersResult(
    val count: Int,
    val limit: Int,
    val offset: Int,
    @SerializedName("results")
    val characterList: List<CharacterModel>,
    val total: Int
)