package com.example.marvelapp.data.model

import java.io.Serializable

data class StoryItem(
    val name: String,
    val resourceURI: String,
    val type: String
) : Serializable