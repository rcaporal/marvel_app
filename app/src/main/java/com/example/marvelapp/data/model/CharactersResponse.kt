package com.example.marvelapp.data.model

import com.google.gson.annotations.SerializedName

data class CharactersResponse(
    @SerializedName("data")
    val response: CharactersResult
)