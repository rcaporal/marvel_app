package com.example.marvelapp.data.model

import java.io.Serializable

data class Stories(
    val available: Int,
    val collectionURI: String,
    val items: List<StoryItem>,
    val returned: Int
) : Serializable