package com.example.marvelapp.data.model

class Keys(
    val privateKey: String,
    val publicKey: String
)
