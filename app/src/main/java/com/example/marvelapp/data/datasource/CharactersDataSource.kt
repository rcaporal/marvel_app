package com.example.marvelapp.data.datasource

import android.content.Context
import androidx.paging.PageKeyedDataSource
import com.example.marvelapp.data.api.MarvelAPI
import com.example.marvelapp.data.model.CharacterModel
import com.example.marvelapp.data.model.Keys
import com.example.marvelapp.util.getKeys
import com.example.marvelapp.util.toMd5
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch


class CharactersDataSource(
    private val context: Context,
    private val scope: CoroutineScope,
    private val api: MarvelAPI,
    private val onApiError: (exception: Exception)->Unit) :
    PageKeyedDataSource<Int, CharacterModel>() {

    companion object{
        const val PAGE_SIZE: Int = 50
        const val FIRST_PAGE: Int = 1
    }

    private lateinit var keys: Keys

    init {
        try {
            keys = getKeys(context)
        }catch (e:Exception){
            onApiError(e)
        }
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, CharacterModel>
    ) {
        scope.launch {
            try {
                val ts = System.currentTimeMillis().toString()
                val hash = listOf(ts, keys.privateKey, keys.publicKey).toMd5() ?: ""
                val response = api.webservice.getCharacters(ts, keys.publicKey, hash, PAGE_SIZE, 0)
                callback.onResult(response.response.characterList, null ,FIRST_PAGE)
            }catch (e:Exception){
                onApiError(e)
            }

        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, CharacterModel>) {
        scope.launch {
            try {
                val offset = params.key * PAGE_SIZE
                val ts = System.currentTimeMillis().toString()
                val hash = listOf(ts, keys.privateKey, keys.publicKey).toMd5() ?: ""
                val response = api.webservice.getCharacters(ts, keys.publicKey, hash, PAGE_SIZE, offset)

                val totalItems = response.response.total
                val returnedItems = response.response.count
                val hasMore = totalItems > returnedItems

                val adjacentKey = if(hasMore)  params.key + 1 else null

                callback.onResult(response.response.characterList, adjacentKey)
            }catch (e: Exception){
                onApiError(e)
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, CharacterModel>) {
        scope.launch {
            try {
                val offset = params.key * PAGE_SIZE
                val ts = System.currentTimeMillis().toString()
                val hash = listOf(ts, keys.privateKey, keys.publicKey).toMd5() ?: ""
                val response = api.webservice.getCharacters(ts, keys.publicKey, hash, PAGE_SIZE, offset)

                val adjacentKey = if(params.key > 1)  params.key - 1 else null

                callback.onResult(response.response.characterList, adjacentKey)
            }catch (e: Exception){
                onApiError(e)
            }
        }
    }

    override fun invalidate() {
        super.invalidate()
        scope.cancel()
    }

}