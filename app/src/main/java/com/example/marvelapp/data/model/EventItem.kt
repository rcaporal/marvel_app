package com.example.marvelapp.data.model

import java.io.Serializable

data class EventItem(
    val name: String,
    val resourceURI: String
) : Serializable