package com.example.marvelapp.data.repository

import com.example.marvelapp.data.model.CharacterModel

interface CharactersRepository {
    suspend fun getCharacters(): List<CharacterModel>?
    suspend fun getCharacterById(characterId: String): CharacterModel?
}