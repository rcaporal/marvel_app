package com.example.marvelapp.data.model

import java.io.Serializable

data class Url(
    val type: String,
    val url: String
) : Serializable