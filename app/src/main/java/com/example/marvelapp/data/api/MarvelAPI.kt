package com.example.marvelapp.data.api

import com.example.marvelapp.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MarvelAPI {

    private val baseUrl: String = "https://gateway.marvel.com/v1/public/"

    val webservice: MarvelService by lazy {
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build().create(MarvelService::class.java)
    }

    private val client: OkHttpClient by lazy {

        val builder = OkHttpClient.Builder().apply {
            if (BuildConfig.DEBUG){
                val logging = HttpLoggingInterceptor()
                logging.level = HttpLoggingInterceptor.Level.BODY
                addInterceptor(logging)
            }
        }

        builder.build()
    }
 }