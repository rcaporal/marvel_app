package com.example.marvelapp.data.model

import java.io.Serializable

data class Series(
    val available: Int,
    val collectionURI: String,
    val items: List<SeriesItem>,
    val returned: Int
) : Serializable