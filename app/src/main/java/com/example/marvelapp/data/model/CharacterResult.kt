package com.example.marvelapp.data.model

data class CharacterResult(
    val results: List<CharacterModel>
)