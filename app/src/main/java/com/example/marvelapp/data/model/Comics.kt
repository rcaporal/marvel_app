package com.example.marvelapp.data.model

import java.io.Serializable

data class Comics(
    val available: Int,
    val collectionURI: String,
    val items: List<ComicsItem>,
    val returned: Int
) : Serializable