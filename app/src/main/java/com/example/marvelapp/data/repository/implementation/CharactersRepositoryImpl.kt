package com.example.marvelapp.data.repository.implementation

import android.content.Context
import com.example.marvelapp.data.api.MarvelAPI
import com.example.marvelapp.data.model.CharactersResponse
import com.example.marvelapp.data.model.CharacterModel
import com.example.marvelapp.data.model.Keys
import com.example.marvelapp.data.repository.CharactersRepository
import com.example.marvelapp.util.getKeys
import com.example.marvelapp.util.toMd5
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class CharactersRepositoryImpl(private val context: Context, private val api: MarvelAPI) :
    CharactersRepository {

    override suspend fun getCharacters(): List<CharacterModel>? {
        var result: CharactersResponse? = null

        withContext(Dispatchers.IO) {
            launch {
                val keys = getKeys(context)
                val ts = System.currentTimeMillis().toString()
                val hash = listOf(ts, keys.privateKey, keys.publicKey).toMd5() ?: ""
                result = api.webservice.getCharacters(ts, keys.publicKey, hash)
            }
        }

        return result?.response?.characterList
    }

    override suspend fun getCharacterById(characterId: String): CharacterModel? {
        var result: CharactersResponse? = null

        withContext(Dispatchers.IO) {
            launch {
                val keys = getKeys(context)
                val ts = System.currentTimeMillis().toString()
                val hash = listOf(ts, keys.privateKey, keys.publicKey).toMd5() ?: ""
                result = api.webservice.getCharacterById(characterId, ts, keys.publicKey, hash)
            }
        }

        return result?.response?.characterList?.first()
    }

}
