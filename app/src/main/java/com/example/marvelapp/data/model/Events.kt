package com.example.marvelapp.data.model

import java.io.Serializable

data class Events(
    val available: Int,
    val collectionURI: String,
    val items: List<EventItem>,
    val returned: Int
) : Serializable