package com.example.marvelapp.data.model

import java.io.Serializable

data class Thumbnail(
    private val extension: String,
    private val path: String
) : Serializable {
    val imageUrl: String get() = "${path}.${extension}"
}